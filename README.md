# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `data-source.ts` file
3. Run `npm start` command

## Notes

```bash
npx typeorm-model-generator -h localhost -d etienne2_wp2 -u user -x secret -e mysql -o tmp/gen/etienne2_wp2/

cd /home/etienne2/chouard.org/www/wp-content/uploads
find -type f -exec md5sum "{}" + | gzip -c > checklist.chk.gz

# then local
cd tmp
scp etienne2@orion:chouard.org/www/wp-content/uploads/checklist.chk.gz ./ ; zcat checklist.chk.gz > checklist.chk

```

```sql
-- cleanup wp_postmeta orphans
DELETE FROM wp_postmeta where meta_id IN (
  select meta_id
  FROM
      `wp_postmeta`
  LEFT JOIN wp_posts p ON
      p.ID = post_id

  where p.ID is null
);



-- cleanup wp_p2p_relationships orphans
DELETE
FROM
    wp_p2p_relationships
WHERE
    rel_id IN (
        SELECT
            `rel_id`
        FROM
            `wp_p2p_relationships` r
        LEFT JOIN wp_posts pf ON
            (r.from_id = pf.ID)
        LEFT JOIN wp_posts pt ON
            (r.to_id = pt.ID)
        WHERE
            pf.ID IS NULL OR pt.ID IS NULL
    )

-- wp_term_relationships orphan
DELETE
FROM
    wp_term_relationships
WHERE
    object_id IN(
    SELECT
        `object_id`
    FROM
        `wp_term_relationships` tr
    LEFT JOIN wp_posts p ON
        p.ID = tr.object_id
    WHERE
        p.ID IS NULL
);


-- populate upload_files mime_type
UPDATE
    `upload_files`
SET
    `mime_type` = NULL;

UPDATE
    `upload_files`
SET
    mime_type = 'image/jpeg'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.jpg' OR LOWER(path) LIKE '%.jpeg'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'image/png'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.png'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'image/bmp'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.bmp'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'image/gif'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.gif'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'image/svg+xml'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.svg'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'image/x-icon'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.ico'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'image/webp'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.webp'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/pdf'
WHERE
    mime_type IS NULL AND(
        LOWER(path) LIKE '%.pdf'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/vnd.ms-powerpoint'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.ppt'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.pptx'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/msword'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.doc'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.docx'
    );
UPDATE
    `upload_files`
SET
    mime_type = 'text/csv'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.csv'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'text/css'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.css'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'text/html'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.htm'
        OR LOWER(path) LIKE '%.html'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/xml'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.xml'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/rtf'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.rtf'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/epub+zip'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.epub'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/zip'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.zip'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'text/plain'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.txt'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'text/plain'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.htaccess'
        OR LOWER(path) LIKE '%.config'
        OR LOWER(path) LIKE '%.log'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'text/php'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.php'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'audio/mpeg'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.mp3'
        OR LOWER(path) LIKE '%.mp2'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'video/mpeg'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.mpg'
        OR LOWER(path) LIKE '%.mpeg'
        OR LOWER(path) LIKE '%.mpe'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'video/mp4'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.mp4'
    );

UPDATE
    `upload_files`
SET
    mime_type = 'application/json'
WHERE
    mime_type IS NULL AND
    (
        LOWER(path) LIKE '%.json'
    );

-- are missing most open formats :'(
-- .odp	présentation OpenDocument	application/vnd.oasis.opendocument.presentation
-- .ods	feuille de calcul OpenDocument	application/vnd.oasis.opendocument.spreadsheet
-- .odt	document texte OpenDocument	application/vnd.oasis.opendocument.text
-- .oga	fichier audio OGG	audio/ogg
-- .ogv	fichier vidéo OGG	video/ogg
-- .ogx	OGG	application/ogg
```

```bash

java -jar ~/bin/schemaspy-6.1.0.jar -t mysql -vizjs -dp ~/bin/mysql-connector-java-8.0.29.jar -db etienne2_wp2 -s etienne2_wp2 -host localhost -u "$DB_USERNAME" -p "$DB_PASSWORD" -o ./tmp/schemaspy

```
