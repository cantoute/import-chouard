
-- DB etienne_wp = prod
-- DB etienne2_wp2 = chouard2
-- DB etienne2_wp = fusion des deux

DELETE FROM etienne2_wp.wp_posts;
DELETE FROM etienne2_wp.wp_terms;

-- -- get all not post+page+attachment+plan_p+auteur+citation+livre from prod
-- INSERT INTO etienne2_wp.wp_posts
-- SELECT
--     p.*
-- FROM
--     etienne_wp.wp_posts p
-- WHERE
--     p.post_status IN('publish', 'draft', 'pending', 'inherit')
--     AND p.post_type IN('post', 'page', 'attachment');


-- get all post+page+attachment from prod 
INSERT INTO etienne2_wp.wp_posts
SELECT
    p.*
FROM
    etienne_wp.wp_posts p
WHERE
    p.post_status IN('publish', 'draft', 'pending', 'inherit') AND p.post_type IN('post', 'page', 'attachment');

-- and their meta
-- INSERT INTO etienne2_wp.wp_postmeta
-- SELECT
--     pm.*
-- FROM
--     etienne_wp.wp_postmeta pm
-- JOIN
--     etienne2_wp.wp_posts p2
--     ON p2.ID = pm.post_id;


-- update content more recent in etienne2_wp2
UPDATE
    etienne2_wp.wp_posts p2
JOIN etienne2_wp2.wp_posts p22
ON
    p22.ID = p2.ID
SET
    p2.`post_author` = p22.post_author,
    p2.`post_date` = p22.post_date,
    p2.`post_date_gmt` = p22.post_date_gmt,
    p2.`post_content` = p22.post_content,
    p2.`post_title` = p22.post_title,
    p2.`post_excerpt` = p22.post_excerpt,
    p2.`post_status` = p22.post_status,
    p2.`comment_status` = p22.comment_status,
    p2.`ping_status` = p22.ping_status,
    p2.`post_password` = p22.post_password,
    p2.`post_name` = p22.post_name,
    p2.`to_ping` = p22.to_ping,
    p2.`pinged` = p22.pinged,
    p2.`post_modified` = p22.post_modified,
    p2.`post_modified_gmt` = p22.post_modified_gmt,
    p2.`post_content_filtered` = p22.post_content_filtered,
    p2.`post_parent` = p22.post_parent,
    p2.`guid` = p22.guid,
    p2.`menu_order` = p22.menu_order,
    p2.`post_type` = p22.post_type,
    p2.`post_mime_type` = p22.post_mime_type
    -- p2.`comment_count`
WHERE
    -- p22.post_modified > p2.post_modified;
    (
      p22.post_modified > p2.post_modified
      -- AND p22.post_type != p2.post_type
      -- AND NOT p22.post_status IN('trash')
      AND p22.post_type IN('plan_p','post','attachment')
      AND p22.ID NOT IN (
        10987 -- newletter-gestion we keep the prod version
      )
    );

-- insert all posts in chouard2 missing from prod
INSERT INTO etienne2_wp.wp_posts
(
SELECT p22.*
FROM
  etienne2_wp2.wp_posts p22
  LEFT JOIN etienne2_wp.wp_posts p2bis
  ON p22.ID = p2bis.ID
WHERE 
  p2bis.ID IS NULL
);


-- more selective
-- SELECT
--     p2.post_type,
--     p2.post_name,
--     p22.post_type,
--     p22.post_name
-- FROM
--     etienne2_wp.wp_posts p2
-- JOIN etienne2_wp2.wp_posts p22
-- ON
--     p22.ID = p2.ID
-- WHERE
--     p22.post_modified > p2.post_modified AND p22.post_type != p2.post_type AND NOT p22.post_status IN('trash') AND p22.post_type IN('plan_p');


-- get their comments
INSERT INTO etienne2_wp.wp_comments
SELECT
    c.*
FROM
    etienne_wp.wp_comments c
JOIN etienne2_wp.wp_posts p2
ON
    p2.ID = c.comment_post_id;

-- and related meta
INSERT INTO etienne2_wp.wp_commentmeta
SELECT
    cm.*
FROM
    etienne_wp.wp_commentmeta cm
JOIN etienne2_wp.wp_comments c2
ON
    c2.comment_id = cm.comment_id;


-- terms from etienne2_wp2
INSERT INTO etienne2_wp.wp_terms
SELECT
    t2.*
FROM
    etienne2_wp2.wp_terms t2;

INSERT INTO etienne2_wp.wp_termmeta
SELECT
    tm2.*
FROM
    etienne2_wp2.wp_termmeta tm2;

INSERT INTO etienne2_wp.wp_term_taxonomy
SELECT
    tt2.*
FROM
    etienne2_wp2.wp_term_taxonomy tt2;

INSERT INTO etienne2_wp.wp_term_relationships
SELECT
    tr.*
FROM
    etienne_wp.wp_term_relationships tr
JOIN etienne2_wp.wp_term_taxonomy tt2
ON
    tt2.term_taxonomy_id = tr.term_taxonomy_id
JOIN etienne2_wp.wp_posts p2
ON
    p2.ID = tr.object_id;


-- get updated relations from chouard2
INSERT INTO etienne2_wp.wp_term_relationships
SELECT
    tr22.*
FROM
    etienne2_wp2.wp_term_relationships tr22
    -- relations in chouard2
    -- on posts we have
JOIN etienne2_wp.wp_posts p2
ON
    p2.ID = tr22.object_id
    -- on taxonomies we have
JOIN etienne2_wp.wp_term_taxonomy tt2
ON
    tt2.term_taxonomy_id = tr22.term_taxonomy_id
    -- excluding relations we have allready
LEFT JOIN etienne2_wp.wp_term_relationships tr2
ON
    (
        tr2.term_taxonomy_id = tr22.term_taxonomy_id AND tr2.object_id = tr22.object_id
    )
WHERE
    tr2.term_taxonomy_id IS NULL;


-- insert relections that have matching posts (not orphans)
INSERT INTO etienne2_wp.wp_p2p_relationships
SELECT
    p2p22.*
FROM
    etienne2_wp2.wp_p2p_relationships p2p22
LEFT JOIN etienne2_wp.wp_posts p2to
ON
    p2p22.to_id = p2to.ID
LEFT JOIN etienne2_wp.wp_posts p2from
ON
    p2p22.from_id = p2from.ID
WHERE
    (NOT p2to.ID IS NULL)
    AND (NOT p2from.ID IS NULL);
