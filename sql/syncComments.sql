DELETE
FROM
    etienne2_wp2.wp_commentmeta
WHERE
    1;

DELETE
FROM
    etienne2_wp2.wp_comments
WHERE
    1;

INSERT INTO etienne2_wp2.`wp_comments`(
    SELECT
        c.*
    FROM
        etienne_wp.wp_comments c
    JOIN etienne2_wp2.wp_posts p2
    ON
        c.comment_post_id = p2.ID
);

-- (
--   SELECT AUTO_INCREMENT
--   FROM information_schema.tables
--   WHERE table_name = 'wp_comments'
--   AND table_schema = 'etienne_wp'
-- );

-- ALTER TABLE etienne2_wp2.`wp_comments` AUTO_INCREMENT = ##;


INSERT INTO etienne2_wp2.`wp_commentmeta`(
    SELECT
        cm.*
    FROM
        etienne_wp.wp_commentmeta cm
    JOIN etienne2_wp2.wp_comments c2
    ON
        c2.comment_id = cm.comment_id
);
