import 'reflect-metadata';
import { DataSource } from 'typeorm';
import { PostFiles } from './entities/PostFiles';
import { PostThumbnail } from './entities/PostThumbnail';
import { UploadFiles } from './entities/UploadFiles';
import { WpCommentmeta } from './entities/WpCommentmeta';
import { WpComments } from './entities/WpComments';
import { WpOptions } from './entities/WpOptions';
import { WpP2pRelationships } from './entities/WpP2pRelationships';
import { WpPostmeta } from './entities/WpPostmeta';
import { WpPosts } from './entities/WpPosts';
import { WpPostViews } from './entities/WpPostViews';
import { WpTermmeta } from './entities/WpTermmeta';
import { WpTermRelationships } from './entities/WpTermRelationships';
import { WpTerms } from './entities/WpTerms';
import { WpTermTaxonomy } from './entities/WpTermTaxonomy';
import { WpUsermeta } from './entities/WpUsermeta';
import { WpUsers } from './entities/WpUsers';
import 'dotenv/config';

export const Etienne2Wp2 = new DataSource({
  type: 'mariadb',
  host: 'localhost',
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: 'etienne2_wp2',
  synchronize: false,
  //   logging: true,
  entities: [
    WpCommentmeta,
    WpComments,
    WpOptions,
    WpP2pRelationships,
    WpPostmeta,
    WpPosts,
    WpPostViews,
    WpTermmeta,
    WpTermRelationships,
    WpTerms,
    WpTermTaxonomy,
    WpUsermeta,
    WpUsers,
    UploadFiles,
    PostFiles,
    PostThumbnail,
  ],
  migrations: [],
  subscribers: [],
});

export const EtienneWp = new DataSource({
  type: 'mariadb',
  host: 'localhost',
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: 'etienne_wp',
  synchronize: false,
  //   logging: true,
  entities: [
    WpCommentmeta,
    WpComments,
    WpOptions,
    WpP2pRelationships,
    WpPostmeta,
    WpPosts,
    WpPostViews,
    WpTermmeta,
    WpTermRelationships,
    WpTerms,
    WpTermTaxonomy,
    WpUsermeta,
    WpUsers,
    UploadFiles,
    PostFiles,
    PostThumbnail,
  ],
  migrations: [],
  subscribers: [],
});
