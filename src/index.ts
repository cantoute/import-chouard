import { Equal, InsertResult, Like, MoreThan, Not } from 'typeorm';
import { Etienne2Wp2, EtienneWp } from './data-source';
import { WpPostmeta } from './entities/WpPostmeta';
import { WpPosts } from './entities/WpPosts';
import { UploadFiles } from './entities/UploadFiles';
import { populateUploadFiles } from './shared/populateUploadFiles';
import { importPostsFromProd } from './shared/importPostsFromProd';
import { updatedPostsFromProd } from './shared/updatedPostsFromProd';

// AppDataSource.initialize().then(async () => {

//     console.log("Inserting a new user into the database...")
//     const user = new User()
//     user.firstName = "Timber"
//     user.lastName = "Saw"
//     user.age = 25
//     await AppDataSource.manager.save(user)
//     console.log("Saved a new user with id: " + user.id)

//     console.log("Loading users from the database...")
//     const users = await AppDataSource.manager.find(User)
//     console.log("Loaded users: ", users)

//     console.log("Here you can setup and run express / fastify / any other framework.")

// }).catch(error => console.log(error))

const init = async () => {
  let app: any = {};

  await Etienne2Wp2.initialize().then(() => {
    console.log('Connected Etienne2Wp2');
  });

  await EtienneWp.initialize().then(() => {
    console.log('Connected EtienneWp');
  });

  return app;
};

const app = init();

app.then(async (app) => {
  console.log('App initiated');

  await populateUploadFiles();

  // await importPostsFromProd();

  await updatedPostsFromProd();
  console.log('Done.');
});

export const syncMedia = async () => {
  const mediaToImport = await EtienneWp.getRepository(WpPosts)
    .createQueryBuilder()
    .select()
    .leftJoinAndSelect('WpPosts.postMeta', 'postMeta')
    .where({
      postType: 'attachment',
      //   postStatus: 'publish',
      postMimeType: Like('image/%'),
      //   postDate: MoreThan('2021-03-26 00:00:00'),
      guid: Not(Equal('')),
    })
    // .orderBy('post_modified', 'DESC')
    // .limit(1)
    .getMany();

  // .slice(0, 3)
  for (const media of mediaToImport) {
    const url = new URL(media.guid);

    const filePath = url.pathname;

    const fileName = filePath.split('/').pop();

    // const fileName = media.guid.split('/').pop();
    const fileDir = /uploads\/(.*?)\/[^\/]+$/.exec(media.guid)[1];

    const searchMedia = await Etienne2Wp2.getRepository(WpPosts)
      .createQueryBuilder()
      .select()
      .where({
        guid: Like(`%${filePath}`),
      })
      .getOneOrFail()
      .catch(async (e) => {
        // console.log(fileDir, fileName);

        const searchMediaFileName = await Etienne2Wp2.getRepository(WpPosts)
          .createQueryBuilder()
          .select()
          .where({
            guid: Like(`%/${fileName}`),
          })
          .getMany()
          .catch(async (e) => {});

        if (searchMediaFileName) {
          if (searchMediaFileName.length === 1) {
            console.log('Found a matching file in different path');
            const newUrl = new URL(searchMediaFileName[0].guid);
            console.log(url.pathname);
            console.log(newUrl.pathname);
          } else {
            console.log('Found MANY matching file in different path', fileName);
          }
        }

        console.log('NOT found a matching file');
      });
  }
};
