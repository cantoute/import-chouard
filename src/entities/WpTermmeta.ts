import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpTerms } from './WpTerms';

@Index('term_id', ['termId'], {})
@Index('meta_key', ['metaKey'], {})
@Entity('wp_termmeta', { schema: 'etienne2_wp2' })
export class WpTermmeta {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'meta_id', unsigned: true })
  metaId: string;

  @Column('bigint', { name: 'term_id', unsigned: true, default: () => "'0'" })
  termId: string;

  @Column('varchar', { name: 'meta_key', nullable: true, length: 255 })
  metaKey: string | null;

  @Column('longtext', { name: 'meta_value', nullable: true })
  metaValue: string | null;

  @ManyToOne(() => WpTerms, (wpTerms) => wpTerms.wpTermmetas, {
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
  })
  @JoinColumn([{ name: 'term_id', referencedColumnName: 'termId' }])
  term: WpTerms;
}
