import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PostFiles } from './PostFiles';
import { WpPostmeta } from './WpPostmeta';
import { WpTermRelationships } from './WpTermRelationships';
import { WpComments } from './WpComments';
import { UploadFiles } from './UploadFiles';
import { PostThumbnail } from './PostThumbnail';
import { WpP2pRelationships } from './WpP2pRelationships';

@Index('type_status_date', ['postType', 'postStatus', 'postDate', 'id'], {})
@Index('post_parent', ['postParent'], {})
@Index('post_author', ['postAuthor'], {})
@Index('post_name', ['postName'], {})
@Index('crp_related', ['postTitle', 'postContent'], { fulltext: true })
@Index('crp_related_title', ['postTitle'], { fulltext: true })
@Index('yarpp_title', ['postTitle'], { fulltext: true })
@Index('yarpp_content', ['postContent'], { fulltext: true })
@Entity('wp_posts', { schema: 'etienne2_wp2' })
export class WpPosts {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'ID', unsigned: true })
  id: string;

  @Column('bigint', {
    name: 'post_author',
    unsigned: true,
    default: () => "'0'",
  })
  postAuthor: string;

  @Column('datetime', {
    name: 'post_date',
    default: () => "'0000-00-00 00:00:00'",
  })
  postDate: Date;

  @Column('datetime', {
    name: 'post_date_gmt',
    default: () => "'0000-00-00 00:00:00'",
  })
  postDateGmt: Date;

  @Column('longtext', { name: 'post_content' })
  postContent: string;

  @Column('text', { name: 'post_title' })
  postTitle: string;

  @Column('text', { name: 'post_excerpt' })
  postExcerpt: string;

  @Column('varchar', {
    name: 'post_status',
    length: 20,
    default: () => "'publish'",
  })
  postStatus: string;

  @Column('varchar', {
    name: 'comment_status',
    length: 20,
    default: () => "'open'",
  })
  commentStatus: string;

  @Column('varchar', {
    name: 'ping_status',
    length: 20,
    default: () => "'open'",
  })
  pingStatus: string;

  @Column('varchar', {
    name: 'post_password',
    length: 255,
    default: () => "''",
  })
  postPassword: string;

  @Column('varchar', { name: 'post_name', length: 200, default: () => "''" })
  postName: string;

  @Column('text', { name: 'to_ping' })
  toPing: string;

  @Column('text', { name: 'pinged' })
  pinged: string;

  @Column('datetime', {
    name: 'post_modified',
    default: () => "'0000-00-00 00:00:00'",
  })
  postModified: Date;

  @Column('datetime', {
    name: 'post_modified_gmt',
    default: () => "'0000-00-00 00:00:00'",
  })
  postModifiedGmt: Date;

  @Column('longtext', { name: 'post_content_filtered' })
  postContentFiltered: string;

  @Column('bigint', {
    name: 'post_parent',
    unsigned: true,
    default: () => "'0'",
  })
  postParent: string;

  @Column('varchar', { name: 'guid', length: 255, default: () => "''" })
  guid: string;

  @Column('int', { name: 'menu_order', default: () => "'0'" })
  menuOrder: number;

  @Column('varchar', { name: 'post_type', length: 20, default: () => "'post'" })
  postType: string;

  @Column('varchar', {
    name: 'post_mime_type',
    length: 100,
    default: () => "''",
  })
  postMimeType: string;

  @Column('bigint', { name: 'comment_count', default: () => "'0'" })
  commentCount: string;

  @OneToMany(() => PostFiles, (postFiles) => postFiles.post)
  postFiles: PostFiles[];

  @OneToMany(() => WpPostmeta, (wpPostmeta) => wpPostmeta.post)
  wpPostmetas: WpPostmeta[];

  @OneToMany(
    () => WpTermRelationships,
    (wpTermRelationships) => wpTermRelationships.object,
  )
  wpTermRelationships: WpTermRelationships[];

  @OneToMany(() => WpComments, (wpComments) => wpComments.commentPost)
  wpComments: WpComments[];

  @OneToMany(() => UploadFiles, (uploadFiles) => uploadFiles.post)
  uploadFiles: UploadFiles[];

  @OneToMany(() => PostThumbnail, (postThumbnail) => postThumbnail.post)
  postThumbnails: PostThumbnail[];

  @OneToMany(
    () => WpP2pRelationships,
    (wpP2pRelationships) => wpP2pRelationships.from,
  )
  wpP2pRelationships: WpP2pRelationships[];

  @OneToMany(
    () => WpP2pRelationships,
    (wpP2pRelationships) => wpP2pRelationships.to,
  )
  wpP2pRelationships2: WpP2pRelationships[];
}
