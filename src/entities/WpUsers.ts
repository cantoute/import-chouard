import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('user_login_key', ['userLogin'], {})
@Index('user_nicename', ['userNicename'], {})
@Index('user_email', ['userEmail'], {})
@Entity('wp_users', { schema: 'etienne2_wp2' })
export class WpUsers {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'ID', unsigned: true })
  id: string;

  @Column('varchar', { name: 'user_login', length: 60, default: () => "''" })
  userLogin: string;

  @Column('varchar', { name: 'user_pass', length: 255, default: () => "''" })
  userPass: string;

  @Column('varchar', { name: 'user_nicename', length: 50, default: () => "''" })
  userNicename: string;

  @Column('varchar', { name: 'user_email', length: 100, default: () => "''" })
  userEmail: string;

  @Column('varchar', { name: 'user_url', length: 100, default: () => "''" })
  userUrl: string;

  @Column('datetime', {
    name: 'user_registered',
    default: () => "'0000-00-00 00:00:00'",
  })
  userRegistered: Date;

  @Column('varchar', {
    name: 'user_activation_key',
    length: 255,
    default: () => "''",
  })
  userActivationKey: string;

  @Column('int', { name: 'user_status', default: () => "'0'" })
  userStatus: number;

  @Column('varchar', { name: 'display_name', length: 250, default: () => "''" })
  displayName: string;
}
