import { Column, Entity, Index } from 'typeorm';

@Index('id_period', ['id', 'period'], { unique: true })
@Index('type_period_count', ['type', 'period', 'count'], {})
@Entity('wp_post_views', { schema: 'etienne2_wp2' })
export class WpPostViews {
  @Column('bigint', { primary: true, name: 'id', unsigned: true })
  id: string;

  @Column('tinyint', { primary: true, name: 'type', unsigned: true })
  type: number;

  @Column('varchar', { primary: true, name: 'period', length: 8 })
  period: string;

  @Column('bigint', { name: 'count', unsigned: true })
  count: string;
}
