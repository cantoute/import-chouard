import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { WpPosts } from './WpPosts';
import { WpTermTaxonomy } from './WpTermTaxonomy';

@Index('term_taxonomy_id', ['termTaxonomyId'], {})
@Entity('wp_term_relationships', { schema: 'etienne2_wp2' })
export class WpTermRelationships {
  @Column('bigint', {
    primary: true,
    name: 'object_id',
    unsigned: true,
    default: () => "'0'",
  })
  objectId: string;

  @Column('bigint', {
    primary: true,
    name: 'term_taxonomy_id',
    unsigned: true,
    default: () => "'0'",
  })
  termTaxonomyId: string;

  @Column('int', { name: 'term_order', default: () => "'0'" })
  termOrder: number;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.wpTermRelationships, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'object_id', referencedColumnName: 'id' }])
  object: WpPosts;

  @ManyToOne(
    () => WpTermTaxonomy,
    (wpTermTaxonomy) => wpTermTaxonomy.wpTermRelationships,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  @JoinColumn([
    { name: 'term_taxonomy_id', referencedColumnName: 'termTaxonomyId' },
  ])
  termTaxonomy: WpTermTaxonomy;
}
