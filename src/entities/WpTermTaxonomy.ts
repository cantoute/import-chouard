import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpTermRelationships } from './WpTermRelationships';
import { WpTerms } from './WpTerms';

@Index('term_id_taxonomy', ['termId', 'taxonomy'], { unique: true })
@Index('taxonomy', ['taxonomy'], {})
@Entity('wp_term_taxonomy', { schema: 'etienne2_wp2' })
export class WpTermTaxonomy {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'term_taxonomy_id',
    unsigned: true,
  })
  termTaxonomyId: string;

  @Column('bigint', { name: 'term_id', unsigned: true, default: () => "'0'" })
  termId: string;

  @Column('varchar', { name: 'taxonomy', length: 32, default: () => "''" })
  taxonomy: string;

  @Column('longtext', { name: 'description' })
  description: string;

  @Column('bigint', { name: 'parent', unsigned: true, default: () => "'0'" })
  parent: string;

  @Column('bigint', { name: 'count', default: () => "'0'" })
  count: string;

  @OneToMany(
    () => WpTermRelationships,
    (wpTermRelationships) => wpTermRelationships.termTaxonomy,
  )
  wpTermRelationships: WpTermRelationships[];

  @ManyToOne(() => WpTerms, (wpTerms) => wpTerms.wpTermTaxonomies, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'term_id', referencedColumnName: 'termId' }])
  term: WpTerms;
}
