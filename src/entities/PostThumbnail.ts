import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { WpPosts } from './WpPosts';

@Entity('post_thumbnail', { schema: 'etienne2_wp2' })
export class PostThumbnail {
  @Column('bigint', { primary: true, name: 'post_id', unsigned: true })
  postId: string;

  @Column('int', { primary: true, name: 'upload_file_id' })
  uploadFileId: number;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.postThumbnails, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'post_id', referencedColumnName: 'id' }])
  post: WpPosts;
}
