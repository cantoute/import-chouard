import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpPosts } from './WpPosts';

@Index('post_id', ['postId'], {})
@Entity('upload_files', { schema: 'etienne2_wp2' })
export class UploadFiles {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'sha', length: 255 })
  sha: string;

  @Column('varchar', { name: 'path', length: 512 })
  path: string;

  @Column('bigint', { name: 'post_id', nullable: true, unsigned: true })
  postId: string | null;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.uploadFiles, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'post_id', referencedColumnName: 'id' }])
  post: WpPosts;
}
