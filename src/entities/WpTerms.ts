import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpTermTaxonomy } from './WpTermTaxonomy';
import { WpTermmeta } from './WpTermmeta';

@Index('slug', ['slug'], {})
@Index('name', ['name'], {})
@Entity('wp_terms', { schema: 'etienne2_wp2' })
export class WpTerms {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'term_id', unsigned: true })
  termId: string;

  @Column('varchar', { name: 'name', length: 200, default: () => "''" })
  name: string;

  @Column('varchar', { name: 'slug', length: 200, default: () => "''" })
  slug: string;

  @Column('bigint', { name: 'term_group', default: () => "'0'" })
  termGroup: string;

  @Column('int', { name: 'term_order', default: () => "'0'" })
  termOrder: number;

  @OneToMany(() => WpTermTaxonomy, (wpTermTaxonomy) => wpTermTaxonomy.term)
  wpTermTaxonomies: WpTermTaxonomy[];

  @OneToMany(() => WpTermmeta, (wpTermmeta) => wpTermmeta.term)
  wpTermmetas: WpTermmeta[];
}
