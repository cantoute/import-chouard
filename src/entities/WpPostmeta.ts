import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpPosts } from './WpPosts';

@Index('post_id', ['postId'], {})
@Index('meta_key', ['metaKey'], {})
@Entity('wp_postmeta', { schema: 'etienne2_wp2' })
export class WpPostmeta {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'meta_id', unsigned: true })
  metaId: string;

  @Column('bigint', { name: 'post_id', nullable: true, unsigned: true })
  postId: string | null;

  @Column('varchar', { name: 'meta_key', nullable: true, length: 255 })
  metaKey: string | null;

  @Column('longtext', { name: 'meta_value', nullable: true })
  metaValue: string | null;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.wpPostmetas, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'post_id', referencedColumnName: 'id' }])
  post: WpPosts;
}
