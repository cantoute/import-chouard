import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpPosts } from './WpPosts';

@Index('from_id', ['fromId', 'toId'], { unique: true })
@Index('to_id', ['toId'], {})
@Entity('wp_p2p_relationships', { schema: 'etienne2_wp2' })
export class WpP2pRelationships {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'rel_id', unsigned: true })
  relId: string;

  @Column('varchar', { name: 'rel_key', length: 200 })
  relKey: string;

  @Column('varchar', { name: 'from_type', length: 200 })
  fromType: string;

  @Column('varchar', { name: 'from_name', length: 200 })
  fromName: string;

  @Column('bigint', { name: 'from_id', unsigned: true })
  fromId: string;

  @Column('varchar', { name: 'from_status', length: 20 })
  fromStatus: string;

  @Column('varchar', { name: 'from_lang', length: 20 })
  fromLang: string;

  @Column('varchar', { name: 'to_type', length: 200 })
  toType: string;

  @Column('varchar', { name: 'to_name', length: 200 })
  toName: string;

  @Column('bigint', { name: 'to_id', unsigned: true })
  toId: string;

  @Column('varchar', { name: 'to_status', length: 20 })
  toStatus: string;

  @Column('varchar', { name: 'to_lang', length: 20 })
  toLang: string;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.wpP2pRelationships, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'from_id', referencedColumnName: 'id' }])
  from: WpPosts;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.wpP2pRelationships2, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'to_id', referencedColumnName: 'id' }])
  to: WpPosts;
}
