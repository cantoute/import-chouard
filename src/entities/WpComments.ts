import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { WpPosts } from './WpPosts';
import { WpCommentmeta } from './WpCommentmeta';

@Index('comment_post_ID', ['commentPostId'], {})
@Index('comment_approved_date_gmt', ['commentApproved', 'commentDateGmt'], {})
@Index('comment_date_gmt', ['commentDateGmt'], {})
@Index('comment_parent', ['commentParent'], {})
@Index('comment_author_email', ['commentAuthorEmail'], {})
@Entity('wp_comments', { schema: 'etienne2_wp2' })
export class WpComments {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'comment_ID',
    unsigned: true,
  })
  commentId: string;

  @Column('bigint', { name: 'comment_post_ID', nullable: true, unsigned: true })
  commentPostId: string | null;

  @Column('tinytext', { name: 'comment_author' })
  commentAuthor: string;

  @Column('varchar', {
    name: 'comment_author_email',
    length: 100,
    default: () => "''",
  })
  commentAuthorEmail: string;

  @Column('varchar', {
    name: 'comment_author_url',
    length: 200,
    default: () => "''",
  })
  commentAuthorUrl: string;

  @Column('varchar', {
    name: 'comment_author_IP',
    length: 100,
    default: () => "''",
  })
  commentAuthorIp: string;

  @Column('datetime', {
    name: 'comment_date',
    default: () => "'0000-00-00 00:00:00'",
  })
  commentDate: Date;

  @Column('datetime', {
    name: 'comment_date_gmt',
    default: () => "'0000-00-00 00:00:00'",
  })
  commentDateGmt: Date;

  @Column('text', { name: 'comment_content' })
  commentContent: string;

  @Column('int', { name: 'comment_karma', default: () => "'0'" })
  commentKarma: number;

  @Column('varchar', {
    name: 'comment_approved',
    length: 20,
    default: () => "'1'",
  })
  commentApproved: string;

  @Column('varchar', {
    name: 'comment_agent',
    length: 255,
    default: () => "''",
  })
  commentAgent: string;

  @Column('varchar', {
    name: 'comment_type',
    length: 20,
    default: () => "'comment'",
  })
  commentType: string;

  @Column('bigint', {
    name: 'comment_parent',
    unsigned: true,
    default: () => "'0'",
  })
  commentParent: string;

  @Column('bigint', { name: 'user_id', unsigned: true, default: () => "'0'" })
  userId: string;

  @ManyToOne(() => WpPosts, (wpPosts) => wpPosts.wpComments, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'comment_post_ID', referencedColumnName: 'id' }])
  commentPost: WpPosts;

  @OneToMany(() => WpCommentmeta, (wpCommentmeta) => wpCommentmeta.comment)
  wpCommentmetas: WpCommentmeta[];
}
