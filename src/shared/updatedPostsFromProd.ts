import { LessThan, MoreThan } from 'typeorm';
import { Etienne2Wp2, EtienneWp } from '../data-source';
import { WpComments } from '../entities/WpComments';
import { WpPosts } from '../entities/WpPosts';
import { forkDate } from './constants';

export const updatedPostsFromProd = async () => {
  const qb = EtienneWp.getRepository(WpPosts)
    .createQueryBuilder()
    .select()
    // .leftJoinAndSelect('WpPosts.wpPostmetas', 'wpPostmetas')
    // .leftJoinAndSelect('WpPosts.wpComments', 'wpComments')
    // .leftJoinAndSelect('WpPosts.wpTermRelationships', 'wpTermRelationships')
    // .leftJoinAndSelect('wpTermRelationships.termTaxonomy', 'termTaxonomy')
    // .leftJoinAndSelect('termTaxonomy.term', 'term')
    // .leftJoinAndSelect('term.wpTermmetas', 'wpTermmetas')
    .where({
      postType: 'post',
      postStatus: 'publish',
      postDate: LessThan(forkDate),
    });
  // .limit(1)
  // .orderBy('post_modified', 'DESC')
  // .limit(1)
  // .getMany();

  const total = await qb.getCount();

  const pagination = 100;

  const nbPages = Math.ceil(total / pagination);

  // const posts = await qb.getMany();

  console.log(`Found ${total} posts to sync ${nbPages}`);

  for (let page = 0; page < nbPages; page++) {
    const posts = await qb
      .take(pagination)
      .skip(page * pagination)
      .getMany();

    console.log(`got ${posts.length} posts`);

    for (const post of posts) {
      // post.wpComments
      const updateTarget = await Etienne2Wp2.getRepository(WpPosts)
        .createQueryBuilder()
        .select()
        // .leftJoinAndSelect('WpPosts.wpPostmetas', 'wpPostmetas')
        // .leftJoinAndSelect('WpPosts.wpTermRelationships', 'wpTermRelationships')
        // .leftJoinAndSelect('wpTermRelationships.termTaxonomy', 'termTaxonomy')
        // .leftJoinAndSelect('termTaxonomy.term', 'term')
        // .leftJoinAndSelect('term.wpTermmetas', 'wpTermmetas')
        .where({
          id: post.id,
          // postName: post.postName,
          // postModified: LessThan(post.postModified),
        })
        .getOne();

      if (updateTarget) {
        if (!post.postName.startsWith(updateTarget.postName)) {
          console.warn(`#${post.id} name differ:`);
          console.warn(`${post.postName}`);
          console.warn(`${updateTarget.postName}`);
        }

        if (updateTarget.postModified < post.postModified) {
          console.log(`updating #${post.id}`);

          const update = await Etienne2Wp2.getRepository(WpPosts)
            .createQueryBuilder()
            .update()
            .set(post)
            .where({
              id: post.id,
            })
            .limit(1)
            .execute();
        } else if (updateTarget.postName != post.postName) {
          console.log(`updating #${post.id} name`);

          const update = await Etienne2Wp2.getRepository(WpPosts)
            .createQueryBuilder()
            .update()
            .set({
              postName: post.postName,
            })
            .where({
              id: post.id,
            })
            .limit(1)
            .execute();
        }

        // comments
        const postComments = await EtienneWp.getRepository(WpComments)
          .createQueryBuilder()
          .select()
          .leftJoinAndSelect('WpComments.wpCommentmetas', 'wpCommentmetas')
          .where({
            commentPostId: post.id,
          })
          .getMany();

        const targetComments = await Etienne2Wp2.getRepository(WpComments)
          .createQueryBuilder()
          .select()
          .leftJoinAndSelect('WpComments.wpCommentmetas', 'wpCommentmetas')
          .where({
            commentPostId: post.id,
          })
          .getMany();

        if (postComments == targetComments) {
          console.log(
            `#${post.id} fetched ${postComments.length} comments. Skipping...`,
          );
        } else {
          console.log(
            `#${post.id} fetched ${postComments.length} comments - target has ${targetComments.length}`,
          );
        }

        for (const postComment of postComments) {
          // const targetComment = await Etienne2Wp2.getRepository(WpComments)
          //   .createQueryBuilder()
          //   .select()
          //   .leftJoinAndSelect('WpComments.wpCommentmetas', 'wpCommentmetas')
          //   .where({
          //     commentId: postComment.commentId,
          //     // commentPostId: postComment.commentPostId,
          //     // commentDate: postComment.commentDate
          //   })
          //   .getOne();
          // if (targetComment) {
          //   if (
          //     postComment.commentPostId == targetComment.commentPostId &&
          //     postComment.commentDate.getTime() ==
          //       targetComment.commentDate.getTime()
          //   ) {
          //     console.log(`target commentId same`);
          //   }
          //   if (
          //     postComment.commentDate.getTime() !=
          //     targetComment.commentDate.getTime()
          //   )
          //     console.log(
          //       `target date differ`,
          //       postComment.commentDate,
          //       targetComment.commentDate,
          //     );
          // } else {
          //   console.log(`target commentId differ`);
          // }
        }
      }
    }
  }
};
