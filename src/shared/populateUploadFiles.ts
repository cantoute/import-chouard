import * as fs from 'fs';
import * as readline from 'readline';
import { InsertResult } from 'typeorm';
import { Etienne2Wp2 } from '../data-source';
import { UploadFiles } from '../entities/UploadFiles';

export const populateUploadFiles = async () => {
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  await Etienne2Wp2.manager
    .findOneByOrFail(UploadFiles, {})
    .then(() => {
      console.log('UploadFiles table not empty. Skipping import');
    })
    .catch(async (e) => {
      // table is empty, we try to populate
      console.log('UploadFiles table empty, populating...');
      const fileStream = fs.createReadStream('tmp/checklist.chk');

      const ins: UploadFiles[] = [];

      if (fileStream) {
        const rl = readline.createInterface({
          input: fileStream,
          crlfDelay: Infinity,
        });

        for await (const line of rl) {
          // Each line in input.txt will be successively available here as `line`.

          const r = /^([^\s]+)\s+\.\/(.*)$/;
          const sha = r.exec(line)[1];
          const path = r.exec(line)[2];

          //a[1].replace(/^\.\//, '');
          if (sha && path) {
            ins.push(<UploadFiles>{
              id: null,
              sha,
              path,
            });
          } else {
            console.error('Error:', sha, path);
          }
        }

        console.log('found sha:', ins.length);
        let totalInserted = 0;

        while (ins.length) {
          const inserted: InsertResult = await Etienne2Wp2
            // .getRepository(UploadFiles)
            .createQueryBuilder()
            .insert()
            .into(UploadFiles)
            .values(ins.splice(0, 500))
            // .returning('*')
            .execute();

          totalInserted += inserted.identifiers.length;

          console.log('Inserted:', inserted.identifiers.length, totalInserted);
        }

        console.log('Done: Populated UploadFiles (sha)');
      }
    }); // (UploadFiles).findOneOrFail();
};
