import { DataSource } from 'typeorm';
import { WpPosts } from '../entities/WpPosts';

export const insertPosts = (
  db: DataSource,
  post: Partial<WpPosts>[] | Partial<WpPosts>,
  options: {} = {},
): Promise<any> => {
  const posts = Array.isArray(post) ? post : [post];

  // sorry, this is pur anti-pattern that Promise calls an async
  // but it's real handy to avoid the .then().then() chain hell using await
  // and still use the resolve/reject pattern
  return new Promise(async (resolve, reject) => {
    try {
      for (const post of posts) {
        type Action = 'insert' | 'update' | 'skip' | null;

        let action: Action = 'insert';

        try {
          // extract a bare post (without relations)
          let {
            wpComments,
            wpP2pRelationships,
            wpP2pRelationships2,
            wpPostmetas,
            wpTermRelationships,
            postThumbnails,
            uploadFiles,
            postFiles,
            ...wpPost
          } = post;

          if (wpPost.id) {
            const existSameId = await db
              .getRepository(WpPosts)
              .createQueryBuilder()
              .select()
              .where({
                id: wpPost.id,
                // postType: post.postType,
                // postName: post.postName,
                // postStatus: In(['publish', 'draft']),
              })
              .getOne();

            if (existSameId) {
              if (existSameId.postName == wpPost.postName) {
                if (
                  wpPost.postModified &&
                  new Date(existSameId.postModified) >=
                    new Date(wpPost.postModified)
                ) {
                  // wpPost seems to be more recent
                  action = 'skip';
                } else {
                  // update
                  action = 'update';
                }
              } else {
                // insert with a new ID
                action = 'insert';
                wpPost.id = undefined;
              }
            }
          }

          const qb = db.getRepository(WpPosts).createQueryBuilder();
          let newPostId: string;

          switch (action) {
            case 'insert':
              const insert = await qb.insert().values(wpPost).execute();
              const newPost = insert.generatedMaps[0];
              newPostId = newPost.id;

              console.log(
                `inserted: old#${wpPost.id} new#${newPostId} ${newPost.postName}`,
              );
              break;
            case 'update':
              const update = await qb
                .update()
                .set(wpPost)
                .where({ id: wpPost.id })
                .execute();
              // newPost = <WpPosts>update.generatedMaps[0];
              // console.log(update);
              newPostId = wpPost.id;

              console.log(
                `updated: #${wpPost.id} (${update.affected}) ${wpPost.postName}`,
              );
              break;
            case 'skip':
              console.info(
                `Skipping: proposed update isn't newer #${wpPost.id} ${wpPost.postName}`,
              );
              break;
            default:
              console.error(
                'insert/update post failed',
                wpPost.postName,
                wpPost.id,
              );
          }

          // for (const exist of exists) {
          //   console.warn(
          //     'Found a post with same slug',
          //     exist.id,
          //     exist.postName,
          //     exist.postStatus,
          //   );

          //   // publish, future, draft, pending, private
          //   // in Chouard, we have an extra: reviser
          //   // 200,auto-draft,draft,inherit,pending,private,publish,reviser,sp-standard,trash

          //   if (post.id == exist.id) {
          //     // do nothing, we will update this one
          //     // and delete any other revision
          //     // } else if (
          //     //   false &&
          //     //   [
          //     //     'publish',
          //     //     'pending',
          //     //     'draft',
          //     //     'pending',
          //     //     'private',
          //     //     'reviser',
          //     //   ].includes(exist.postStatus)
          //     // ) {
          //     //   // so the one we found we should do something about it
          //     //   // for now we'll throw an error
          //     //   throw <MyErrors>{
          //     //     message: 'Il exist un article correspondant',
          //     //     data: { post, exist },
          //     //   };
          //   } else {
          //     await db
          //       .getRepository(WpPosts)
          //       .createQueryBuilder()
          //       .delete()
          //       .from(WpPosts)
          //       .where({
          //         id: exist.id,
          //       })
          //       .execute()
          //       .then(() => {
          //         console.log('Deleted:', exist.id);
          //       });
          //   }
          // }
          // if no post share same id we can inject it as is
          // but if id isn't free we need to clean up the post of it's old id

          // let existSameId;

          // if (wpPost.id) {
          //   existSameId = await db
          //     .getRepository(WpPosts)
          //     .createQueryBuilder()
          //     .select()
          //     .where({
          //       id: wpPost.id,
          //       // postName: post.postName,
          //       // postStatus: In(['publish', 'draft']),
          //     })
          //     .getOne();

          //   if (existSameId) {
          //     console.warn(
          //       'exists a post with same id',
          //       existSameId.id,
          //       existSameId.postType,
          //       existSameId.postName,
          //     );
          //     wpPost.id = undefined;
          //   }
          // }

          // const insertedPost = await db
          //   .getRepository(WpPosts)
          //   .createQueryBuilder()
          //   .insert()
          //   .into(WpPosts)
          //   .values(wpPost)
          //   .execute();

          // const newPostId = insertedPost.generatedMaps[0].id;

          // // console.log(insertedPost);

          // if (post.id && post.id != newPostId) {
          //   console.log(`Inserted postId:${newPostId} (oldPostId:${post.id})`);
          // } else {
          //   console.log(`Inserted postId:${newPostId}`);
          // }

          // if (post.wpComments) {
          //   for (const i in post.wpComments) {
          //      post.wpComments[i].commentPostId = undefined;
          //   }
          // }

          // if (post.wpPostmetas) {
          //   for (const i in post.wpPostmetas) {
          //     post.wpPostmetas[i].postId = undefined;
          //   }
          // }

          // if (post.wpTermRelationships) {
          //   for (const i in post.wpTermRelationships) {
          //     post.wpTermRelationships[i].objectId = undefined;
          //     post.wpTermRelationships[i].termTaxonomyId = undefined;
          //     post.wpTermRelationships[i].termTaxonomy.termId = undefined;
          //   }
          // }

          // console.log('Prod:', post.postDate, post.postModified);
          // console.log('Wp2 :', exist.postDate, exist.postModified);

          // console.log(exist);
        } catch (error) {
          console.error(error);
        }
      }

      // we finished import
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};
