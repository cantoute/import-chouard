import { copyFileSync } from 'fs';
import { exit } from 'process';
import { DataSource, In, MoreThan } from 'typeorm';
import { Etienne2Wp2, EtienneWp } from '../data-source';
import { PostFiles } from '../entities/PostFiles';
import { WpPosts } from '../entities/WpPosts';
import { forkDate } from './constants';
import { insertPosts } from './insertPosts';

class MyErrors {
  message: string;
  data?:
    | {
        post?: WpPosts;
        exist?: WpPosts;
      }
    | any;
}

export const importPostsFromProd = async () => {
  const posts = await EtienneWp.getRepository(WpPosts)
    .createQueryBuilder()
    .select()
    .leftJoinAndSelect('WpPosts.wpPostmetas', 'wpPostmetas')
    .leftJoinAndSelect('WpPosts.wpComments', 'wpComments')
    .leftJoinAndSelect('WpPosts.wpTermRelationships', 'wpTermRelationships')
    .leftJoinAndSelect('wpTermRelationships.termTaxonomy', 'termTaxonomy')
    .leftJoinAndSelect('termTaxonomy.term', 'term')
    .leftJoinAndSelect('term.wpTermmetas', 'wpTermmetas')
    .where({
      postType: 'post',
      postStatus: 'publish',
      postDate: MoreThan(forkDate),
    })
    // .limit(1)
    // .orderBy('post_modified', 'DESC')
    // .limit(1)
    .getMany();

  console.log(`Found ${posts.length} posts to import`);

  // console.log(`Ex: `, posts[0]);

  for (const post of posts) {
    // const exists = await Etienne2Wp2.getRepository(WpPosts)
    //   .createQueryBuilder()
    //   .select()
    //   .where({
    //     postType: 'post',
    //     postName: post.postName,
    //     // postStatus: In(['publish', 'draft']),
    //   })
    //   .getMany();

    // for (const exist of exists) {
    //   console.warn(
    //     'Found a post with same slug',
    //     exist.id,
    //     exist.postName,
    //     exist.postStatus,
    //   );

    //   console.log('Prod:', post.postDate, post.postModified);
    //   console.log('Wp2 :', exist.postDate, exist.postModified);
    //   await Etienne2Wp2.getRepository(WpPosts)
    //     .createQueryBuilder()
    //     .delete()
    //     .from(WpPosts)
    //     .where({
    //       id: exist.id,
    //     })
    //     .execute();

    //   console.log('Deleted:', exist.id);
    //   // console.log(exist);
    // }

    const insertedPost = await insertPosts(Etienne2Wp2, post);
  }

  // for (const post of postsToImport.slice(0, 1)) {
  //     // const postImport = await Etienne2Wp2.getRepository(WpPosts).findOneBy({
  //     //   id: post.id,
  //     // });
  //     // console.log(postWp?.postTitle);
  //   }
  //   const oneBis = await EtienneWp.getRepository(WpPosts)
  //     .createQueryBuilder()
  //     .select('WpPosts')
  //     .orderBy('post_modified', 'DESC')
  //     .limit(1)
  //     .getMany();
  //   console.log(one.postTitle, one.id);
  //   console.log(oneBis.postTitle, oneBis.id);
};

//
